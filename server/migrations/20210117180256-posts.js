const faker = require('faker');
const range = require('lodash/range');
const { ObjectId } = require('mongodb');

const createPost = (authorId) => {
  return {
    title: faker.lorem.sentence(),

    content: faker.lorem.text(2048),

    authorId,

    created: faker.date.recent(),

    slug: faker.lorem.slug()

  };
};

module.exports = {
  async up(db, client) {
    const usersCollection = db.collection('users');
    const postsCollection = db.collection('posts');

    usersCollection.find().forEach(async (user) => {
      const posts = range(faker.random.number({ min: 1, max: 10 })).map(() => createPost(new ObjectId(user._id)));
      await postsCollection.insertMany(posts);
      console.log(`Seeding: insert ${posts.length} post items for user ${user._id}`);
    });
  },

  async down(db, client) {
    // TODO write the statements to rollback your migration (if possible)
    // Example:
    // await db.collection('albums').updateOne({artist: 'The Beatles'}, {$set: {blacklisted: false}});
  }
};
