import { Injectable } from '@nestjs/common';
import { MongoMemoryServer } from 'mongodb-memory-server';

@Injectable()
export class DbServer {

  public readonly mongod: MongoMemoryServer;

  constructor() {
    this.mongod = new MongoMemoryServer({
      instance: {
        ip: '127.0.0.1',
        port: 51111,
        dbName: 'user-app',
      }
    });
  }

  public getConnectionString = async (): Promise<string> => {
    await this.mongod.ensureInstance();
    const dbName = await this.mongod.getDbName();
    const port = await this.mongod.getPort();
    console.log(dbName, port);
    return this.mongod.getUri('user-app');
  }

}
