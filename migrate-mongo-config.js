module.exports = {
  mongodb: {
    url: "mongodb://127.0.0.1:51111",

    databaseName: "user-app",

    options: {
      useNewUrlParser: true
    }
  },

  migrationsDir: "server/migrations",

  changelogCollectionName: "changelog",

  migrationFileExtension: ".js"
};
